# ZZP Kompas
-concept-

## Met een focus op IT Software development
...

## Lijst met inhuur platformen van organisaties

### 1. (Semi)Overheid:

| Naam                                               | Inhuurplatform                                                                                                                                                                                           | Careers                                                          | Website |
|----------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------|---------|
| Brabantse Delta (Waterschap)                       | https://www.tenderned.nl/cms/                                                                                                                                                                            | https://www.brabantsedelta.nl/werken-bij-ons/werken-bij-ons.html |         |
| Centraal Justitieel Incassobureau                  | https://www.sso-noord.nl/                                                                                                                                                                                |                                                                  |         |
| DUO                                                | https://www.sso-noord.nl                                                                                                                                                                                 |                                                                  |         |
| KvK                                                | https://platform.negometrix.com/                                                                                                                                                                         |                                                                  |         |
| EU Supply                                          | https://www.eu-supply.com                                                                                                                                                                                |                                                                  |         |
| GVB Amsterdam                                      | https://www.inhuurdeskgvb.nl                                                                                                                                                                             |                                                                  |         |
| Hollandse Delta (Waterschap)                       | https://platform.negometrix.com/                                                                                                                                                                         |                                                                  |         |
| Hoogheemraadschap de Stichtse Rijnlanden           | https://flextender.nl/                                                                                                                                                                                   |                                                                  |         |
| IGOM (Zuid- en Midden Limburgse (semi-)overheid)   | https://inhuurdesk.igom.nl/                                                                                                                                                                              |                                                                  |         |
| InhuurNoord (Prov Groningen, Prov Drenthe, Lentis) | http://www.inhuurnoord.nl/                                                                                                                                                                               |                                                                  |         |
| Inspectie van het Onderwijs                        | https://www.sso-noord.nl/                                                                                                                                                                                |                                                                  |         |
| Milieudienst Rijnmond DCMR                         | https://platform.negometrix.com                                                                                                                                                                          |                                                                  |         |
| ICTU                                               | https://www.ictu.nl/werken-bij-voor/flexibel-werken-bij-ictu (Maakt gebruik van brokers: https://hinttech.com/opdrachten/, https://between.com, https://www.myler.nl)                                    |                                                                  |         |
| Ministerie van Defensie                            | https://www.ctmsolution.nl/marktplaats/                                                                                                                                                                  |                                                                  |         |
| Politie                                            | http://www.inhuurdeskpolitie.nl/                                                                                                                                                                         |                                                                  |         |
| Nederlandse Spoorwegen Inhuurdesk NS               | https://inhuur-ns.secure.force.com/                                                                                                                                                                      |                                                                  |         |
| NL in Business                                     | http://inhuurdesknlinbusiness.nl/                                                                                                                                                                        |                                                                  |         |
| Omgevingsdienst Midden-Holland                     | http://www.inhuurdeskodmh.nl/                                                                                                                                                                            |                                                                  |         |
| Omgevingsdienst Regio Nijmegen (ODRN)              | https://www.werkeningelderland.nl/inhuur/                                                                                                                                                                |                                                                  |         |
| Omgevingsdienst West-Holland                       | http://www.inhuurdeskodwh.nl/                                                                                                                                                                            |                                                                  |         |
| Peel en Maasvallei (Waterschap)                    | https://marktplaats.banenpleinlimburg.nl/                                                                                                                                                                |                                                                  |         |
| Raad voor Rechtsbijstand                           | https://www.ctmsolution.nl/marktplaats/                                                                                                                                                                  |                                                                  |         |
| Regio Overijssel, Drenthe, Groningen               | http://www.marktplaatsdesk.nl                                                                                                                                                                            |                                                                  |         |
| Rijkswaterstaat                                    | https://job.ubrijk.nl/hc/nl (info: https://www.rijkswaterstaat.nl/zakelijk/zakendoen-met-rijkswaterstaat/werkwijzen/werkwijze-in-gww/contracten-gww/marktplaats-voor-inhuur-extern-personeel/index.aspx) |                                                                  |         |
| Rivm                                               | https://platform.negometrix.com/                                                                                                                                                                         |                                                                  |         |
| UWV Marktplaats                                    | https://uwv.secure.force.com                                                                                                                                                                             |                                                                  |         |
| Zorginstituut Nederland                            | https://www.ctmsolution.nl/marktplaats/                                                                                                                                                                  |                                                                  |         |


### 2. Gemeenten en Provincies:
| Naam                                       | Inhuurplatform                                                                  | Careers                                       | Website |
|--------------------------------------------|---------------------------------------------------------------------------------|-----------------------------------------------|---------|
| Alkmaar / Stadswerk072                     | http://inhuurdeskstadswerk072.nl/                                               |                                               |         |
| Alphen aan den Rijn Inhuurdesk             | http://www.inhuurdeskalphenaandenrijn.nl/                                       |                                               |         |
| Amersfoort                                 | https://www.gemeenteamersfoorthuurtin.nl/                                       |                                               |         |
| Amstelveen                                 | https://www.amstelveenhuurtin.nl/                                               |                                               |         |
| Amsterdam                                  | http://www.amsterdam.nl/ondernemen/inkoop-aanbesteden/externe-inhuur/           |                                               |         |
| Apeldoorn                                  | https://www.werkeningelderland.nl/inhuur/                                       |                                               |         |
| Arnhem                                     | https://www.werkeningelderland.nl/inhuur/                                       |                                               |         |
| Bergen op Zoom                             | http://www.flexwestbrabant.nl/                                                  |                                               |         |
| Bodegraven-Reeuwijk                        | http://www.inhuurdeskbodegraven-reeuwijk.nl/                                    |                                               |         |
| Boxmeer (Land van Cuijk gemeenten)         | http://www.boxmeer.nl/website/externe-inhuur_41089/                             |                                               |         |
| Bergen                                     | http://www.flextender.nl                                                        |                                               |         |
| Breda                                      | http://www.flexwestbrabant.nl/                                                  |                                               |         |
| Bunschoten                                 | http://www.flextender.nl                                                        |                                               |         |
| Bussum                                     | http://www.flextender.nl                                                        |                                               |         |
| Capelle ad IJssel                          | https://www.tijdelijkwerkaandenijssel.nl/                                       |                                               |         |
| Drechtsteden                               |                                                                                 |                                               |         |
| Delft                                      | https://www.ctmsolution.nl/marktplaats/                                         |                                               |         |
| Den Bosch                                  | https://inhuurdesk.werkeninnoordoostbrabant.nl/                                 |                                               |         |
| Den Haag                                   | http://www.inhuurdeskdenhaag.nl/                                                |                                               |         |
| Deventer                                   | https://www.talentenregiohuurtin.nl/                                            |                                               |         |
| Doetichem                                  | https://www.werkeningelderland.nl/inhuur/                                       |                                               |         |
| Domesta                                    | http://www.inhuurdeskdomesta.nl/                                                |                                               |         |
| Drachten / Smallingerland                  | http://www.werkeninfriesland.nl/inhuur-externen/                                |                                               |         |
| Dronten                                    | https://www.flevolandhuurtin.nl/                                                |                                               |         |
| Ede                                        | https://www.werkeningelderland.nl/inhuur/                                       |                                               |         |
| Etten-Leur                                 | http://www.flexwestbrabant.nl/                                                  |                                               |         |
| Haarlemmerliede                            | http://www.inhuurdeskhaarlemmerliede.nl/                                        |                                               |         |
| Haarlemmermeer (Hoofddorp)                 | https://haarlemmermeerhuurtin.nl/                                               |                                               |         |
| Heerenveen                                 | https://www.ctmsolution.nl/project/heerenveen/                                  |                                               |         |
| Hilversum                                  | http://www.flextender.nl                                                        |                                               |         |
| Horst aan de Maas                          | https://marktplaats.banenpleinlimburg.nl/                                       |                                               |         |
| Huizen                                     | http://www.flextender.nl                                                        |                                               |         |
| Kampen                                     | http://www.flextender.nl                                                        |                                               |         |
| Leeuwarden                                 | http://www.werkeninfriesland.nl/inhuur-externen/                                |                                               |         |
| Leidschendam-Voorburg                      | http://www.flextender.nl                                                        |                                               |         |
| Lelystad                                   | https://www.flevolandhuurtin.nl/                                                |                                               |         |
| Limburg                                    | https://marktplaats.banenpleinlimburg.nl//                                      |                                               |         |
| Nijkerk                                    | http://www.flextender.nl                                                        |                                               |         |
| Nijmegen                                   | http://www.werkeningelderland.nl/inhuur                                         |                                               |         |
| Provincie Brabant                          | http://www.werkeninnoordoostbrabant.nl/, http://www.werkeninzuidoostbrabant.nl/ |                                               |         |
| Provincie Drenthe                          | http://www.marktplaatsdesk.nl/                                                  |                                               |         |
| Provincie Friesland                        | http://www.werkeninfriesland.nl/inhuur-externen/                                |                                               |         |
| Provincie Gelderland                       | https://www.werkeningelderland.nl/inhuur/                                       |                                               |         |
| Provincie Limburg                          | https://platform.negometrix.com                                                 |                                               |         |
| Provincie Overijssel                       | http://www.overijssel.nl/overijssel/ambtelijke/inzetdesk/                       |                                               |         |
| Provincie Zuid-Holland                     | http://www.ctmsolution.nl/project/provincie-zuid-holland/                       |                                               |         |
| Roermond                                   | https://marktplaats.banenpleinlimburg.nl/                                       |                                               |         |
| Roosendaal                                 | http://www.flexwestbrabant.nl/                                                  |                                               |         |
| Rotterdam                                  | https://www.rotterdam.nl/werken-leren/das-externe-inhuur/                       |                                               |         |
| Urk                                        | https://www.flevolandhuurtin.nl/                                                |                                               |         |
| Utrecht (Gemeente)                         | https://www.werkenbijutrecht.nl/Home/Tenders                                    |                                               |         |
| Utrecht (Provincie)                        | http://inhuurdeskprovincie-utrecht.nl/                                          |                                               |         |
| Utrechtse Heuvelrug (gemeente)             | https://www.heuvelrug.nl/gemeente/pilot-inhuur-personeel-via-das_49397/         |                                               |         |
| Terschelling                               | http://www.werkeninfriesland.nl/inhuur-externen/                                |                                               |         |
| Tiel                                       | https://www.werkeningelderland.nl/inhuur/                                       |                                               |         |
| Twente (regio)                             |                                                                                 |                                               |         |
| Uden                                       | https://vacatures.werkeninnoordoostbrabant.nl                                   |                                               |         |
| Waddinxveen                                | http://www.inhuurdeskgroenehart.nl/                                             |                                               |         |
| Weert                                      | https://marktplaats.banenpleinlimburg.nl/                                       |                                               |         |
| Weesp                                      | https://www.flextender.nl                                                       |                                               |         |
| Wijdemeren                                 | https://www.flextender.nl                                                       |                                               |         |
| Woerden                                    | http://www.inhuurdeskwoerden.nl/                                                |                                               |         |
| Wolden                                     | http://www.marktplaatsdesk.nl                                                   |                                               |         |
| Zoetermeer                                 | https://www.flextender.nl/                                                      |                                               |         |
| BEL (Blaricum, Eemnes, Laren)              | https://www.flextender.nl/                                                      |                                               |         |
| Enschede                                   | https://platform.negometrix.com                                                 |                                               |         |
| Flex-West-Brabant                          | http://www.flexwestbrabant.nl/                                                  |                                               |         |
| Gennep                                     | https://marktplaats.banenpleinlimburg.nl/                                       |                                               |         |
| Goeree-Overflakkee                         | http://www.flextender.nl                                                        |                                               |         |
| Gouda (Marktplaats Groene Hart)            | http://www.inhuurdeskgroenehart.nl/                                             |                                               |         |
| Groningen huurt in.                        | https://www.groningenhuurtin.nl/                                                |                                               |         |
| Harderwijk                                 | https://platform.negometrix.com                                                 |                                               |         |
| Hollands Kroon                             | http://www.inhuurdeskhollandskroon.nl                                           |                                               |         |
| Hoogeveen                                  | http://www.marktplaatsdesk.nl                                                   |                                               |         |
| Hoogezand-Sappemeer                        | http://www.marktplaatsdesk.nl                                                   |                                               |         |
| Hoogheemraadschap Hollands Noorderkwartier | http://www.inhuurhhnk.nl/                                                       |                                               |         |
| Friesland                                  | http://www.inhuurfriesland.nl/                                                  |                                               |         |
| Meppel                                     | http://www.marktplaatsdesk.nl                                                   |                                               |         |
| Naarden                                    | http://www.flextender.nl                                                        |                                               |         |
| Noordoostpolder                            | https://www.flevolandhuurtin.nl/                                                |                                               |         |
| Ommen                                      | http://www.flextender.nl                                                        |                                               |         |
| Oss                                        | https://inhuurdesk.werkeninnoordoostbrabant.nl/                                 |                                               |         |
| Oudewater                                  | http://www.flextender.nl                                                        |                                               |         |
| Rhenen                                     | https://www.werkeningelderland.nl/inhuur/                                       |                                               |         |
| Rijswijk                                   | https://flextender.nl                                                           |                                               |         |
| Slochteren                                 | https://www.marktplaatsdesk.nl                                                  |                                               |         |
| Talentenregio (gemeenten Overijssel e.o.)  | http://www.talentenregiohuurtin.nl/overons                                      |                                               |         |
| Veghel                                     | https://inhuurdesk.werkeninnoordoostbrabant.nl/                                 | https://vacatures.werkeninnoordoostbrabant.nl |         |
| Venlo                                      | https://marktplaats.banenpleinlimburg.nl/                                       |                                               |         |
| Venray                                     | https://marktplaats.banenpleinlimburg.nl/                                       |                                               |         |
| VNG                                        | https://www.ctmsolution.nl/marktplaats/                                         |                                               |         |
| Zuidplas                                   | http://www.inhuurdeskzuidplas.nl/                                               |                                               |         |
| Zutphen                                    | https://www.werkeningelderland.nl/inhuur/                                       |                                               |         |
| Zwolle                                     | http://www.talentenregiohuurtin.nl/overons                                      |                                               |         |


### 3. Zorg
| Naam                     | Inhuurplatform                                               | Careers | Website |
|--------------------------|--------------------------------------------------------------|---------|---------|
| Haga Ziekenhuis Den Haag | http://www.inhuurdeskhaga.nl/                                |         |         |
| St Antonius Ziekenhuis   | http://www.inhuurdesk-stantonius.nl/                         |         |         |
| UMC Utrecht              | http://www.inhuurdeskumcutrecht.nl/                          |         |         |
| VUmc & AMC               | https://werkenbijamc.nl/interim-en-zzp/                      |         |         |
| Medisch Spectrum Twente  | http://www.inhuurdeskmst.nl/                                 |         |         |
| GGD Zuid Limburg         | https://inhuurdesk.igom.nl/Organisatie/GGD-Zuid-Limburg/3164 |         |         |
| GGZ Delfland             | http://www.ggzinhuurdesk.nl/                                 |         |         |
| Radboudumc               | http://www.inhuurdeskradboudumc.nl/                          |         |         |

### 4. Overig
| Naam                           | Inhuurplatform                                                                     | Careers | Website |
|--------------------------------|------------------------------------------------------------------------------------|---------|---------|
| ABNAMRO                        | http://www.abnamro.com/nl/carriere/interim/index.html                              |         |         |
| BAM                            | https://www.bam.com/nl/inkoop/flexplein                                            |         |         |
| Basic-Fit                      | https://hiringdesk.basic-fit.com/                                                  |         |         |
| Erasmus Universiteit Rotterdam | https://platform.negometrix.com/PublishedTenders.aspx?tenderId=9076&companyId=1799 |         |         |
| Hogeschool Leiden              | https://platform.negometrix.com/                                                   |         |         |
| Hogeschool Rotterdam           |                                                                                    |         |         |
| Hogeschool Viaa                | http://www.ctmsolution.nl/project/hogeschool-viaa/                                 |         |         |
| Eneco                          | https://www.werkenbijeneco.nl/werkgebieden/interim-opdrachten-bij-eneco/           |         |         |
| Equens                         | http://inhuurdesk.equens.com/                                                      |         |         |
| Equens (Duitsland)             | http://hiringdesk.equensworldline.com/                                             |         |         |
| Equens (Finland)               | http://hiringdesk.equens.com/                                                      |         |         |
| Equens (Italie)                | http://hiringdesk.equensworldline.com/                                             |         |         |
| ING                            | http://www.ing.jobs/Nederland/Home.htm                                             |         |         |
| Menzis                         | https://www.menzis.nl/werkenbijmenzis/vacatures/interim                            |         |         |
| NN Group                       | https://nn-careers.com/nl/solliciteren-als-interim-professional/                   |         |         |
| Philips                        | https://philips.talent-pool.com/                                                   |         |         |
| Rabobank                       | Niet gevonden. Maakt gebruik van: https://www.headfirst.nl                         |         |         |
| Stedin                         | https://www.stedin.net/werken-bij/werkgebied-inhuur                                |         |         |
| Havenbedrijf Rotterdam         | https://www.portofrotterdam.com/nl/havenbedrijf/carriere-en-vacatures/inhuurdesk   |         |         |
| Tata Steel                     | http://www.tatasteeljobs.nl/interim/werkwijze-bij-inhuur.html                      |         |         |
| Tennet                         | http://www.werkenbijtennet.nl/nl-NL/Vacatures/Interim-opdrachten                   |         |         |
| TU Eindhoven                   |                                                                                    |         |         |
| VodafoneZiggo                  | https://careers.vodafoneziggo.com/contact                                          |         |         |
| PostNL                         | http://marktplaats.interimbijpostnl.nl/                                            |         |         |



### 5. Ongesorteerd
| Naam                                          | Inhuurplatform                                                                          | Careers | Website |
|-----------------------------------------------|-----------------------------------------------------------------------------------------|---------|---------|
| AEB Amsterdam                                 | https://platform.negometrix.com                                                         |         |         |
| Aegon                                         | https://careers.aegon.com/nl/home/zzp/zzp/                                              |         |         |
| Alliander                                     | http://www.werkenbijliander.nl/vacatures/index.htm?search=                              |         |         |
| Aon                                           | http://inhuurdesk.aon.nl/                                                               |         |         |
| Dela                                          | http://www.inhuurdeskdela.nl/                                                           |         |         |
| Dynniq                                        | http://inhuurdeskdynniq.com/                                                            |         |         |
| EY (GigNow)                                   | https://www.gignow.com/                                                                 |         |         |
| Horizon Jeugdzorg en Onderwijs                | http://www.intrakoopinhuurdesk.nl/                                                      |         |         |
| IHC Merwede Staffingdesk                      | http://www.ihcjobs.nl/leveranciers/informatie.html                                      |         |         |
| Imtech Nederland                              | http://inhuurdesk.inl.imtech.com/                                                       |         |         |
| Inkoopsamenwerking Noord-Veluwe (ISNV)        | https://platform.negometrix.com                                                         |         |         |
| Intrakoop  marktplaats voor inhuur in de zorg | http://www.intrakoopinhuurdesk.nl/                                                      |         |         |
| KPMG                                          | http://www.kpmg.com/nl/nl/werken-bij/professionals/pages/zelfstandig-professionals.aspx |         |         |
| Luchtverkeersleiding Nederland (LVNL)         | https://platform.negometrix.com/                                                        |         |         |
| MVGM                                          | http://www.inhuurdeskmvgm.nl/                                                           |         |         |
| NWO                                           | https://platform.negometrix.com/                                                        |         |         |
| Nederlandse Publieke Omroep                   | https://www.ctmsolution.nl/marktplaats/                                                 |         |         |
| Pro Persona                                   | http://inhuurdeskpropersona.nl/                                                         |         |         |
| s Heeren Loo Zorggroep                        | http://www.inhuurdesksheerenloo.nl/                                                     |         |         |
| SURFmarket                                    | https://www.ctmsolution.nl/marktplaats/                                                 |         |         |
| Veiligheidsregio Kennemerland                 | https://www.vrkhuurtin.nl/                                                              |         |         |
